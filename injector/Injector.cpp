#include "Injector.h"
	    
HMODULE Injector::GetRemoteModuleHandleA( DWORD dwProcessId, const char* szModule )
{
	HANDLE th32 = CreateToolhelp32Snapshot( TH32CS_SNAPMODULE, dwProcessId );
	MODULEENTRY32 modEntry;

	modEntry.dwSize = sizeof( MODULEENTRY32 );

	while( Module32Next( th32, &modEntry ) )
	{
		if( _stricmp( szModule, modEntry.szModule ) == 0 )
		{
			CloseHandle( th32 );

			return modEntry.hModule;
		}
	}

	CloseHandle( th32 );

	return NULL;
}

DWORD Injector::GetProcessIdFromProcessName( const char* szProcessName )
{
	HANDLE th32 = CreateToolhelp32Snapshot( TH32CS_SNAPPROCESS, NULL );
	PROCESSENTRY32 procEntry;

	procEntry.dwSize = sizeof( PROCESSENTRY32 );

	while( Process32Next( th32, &procEntry ) )
	{
		if( _stricmp( szProcessName, procEntry.szExeFile ) == 0 )
		{
			CloseHandle( th32 );
			return procEntry.th32ProcessID;
		}
	}

	CloseHandle( th32 );

	return GetCurrentProcessId();
}
