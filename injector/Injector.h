#pragma once

#include <windows.h>
#include <TlHelp32.h>

class Injector
{
public:
	HMODULE GetRemoteModuleHandleA( DWORD dwProcessId, const char* szModule );
	DWORD GetProcessIdFromProcessName( const char* szProcessName );

};

