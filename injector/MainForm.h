#pragma once

#include "Injector.h"
#include <windows.h>

namespace injector {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Runtime::InteropServices;

	/// <summary>
	/// Summary for MainForm
	/// </summary>
	public ref class MainForm : public System::Windows::Forms::Form
	{
	public:
		MainForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MainForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  bntInject;
	protected:

	private: System::Windows::Forms::TextBox^  txtProcess;
	private: System::Windows::Forms::TextBox^  txtDllPath;
	protected:


	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	private: System::Windows::Forms::Label^  txtError;
	private: System::Windows::Forms::Label^  txtSuccess;


	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = ( gcnew System::ComponentModel::ComponentResourceManager( MainForm::typeid ) );
			this->bntInject = ( gcnew System::Windows::Forms::Button() );
			this->txtProcess = ( gcnew System::Windows::Forms::TextBox() );
			this->txtDllPath = ( gcnew System::Windows::Forms::TextBox() );
			this->label1 = ( gcnew System::Windows::Forms::Label() );
			this->label2 = ( gcnew System::Windows::Forms::Label() );
			this->pictureBox1 = ( gcnew System::Windows::Forms::PictureBox() );
			this->txtError = ( gcnew System::Windows::Forms::Label() );
			this->txtSuccess = ( gcnew System::Windows::Forms::Label() );
			( cli::safe_cast< System::ComponentModel::ISupportInitialize^ >( this->pictureBox1 ) )->BeginInit();
			this->SuspendLayout();
			// 
			// bntInject
			// 
			this->bntInject->Location = System::Drawing::Point( 207, 123 );
			this->bntInject->Name = L"bntInject";
			this->bntInject->Size = System::Drawing::Size( 65, 22 );
			this->bntInject->TabIndex = 0;
			this->bntInject->Text = L"Inject";
			this->bntInject->UseVisualStyleBackColor = true;
			this->bntInject->Click += gcnew System::EventHandler( this, &MainForm::bntInject_Click );
			// 
			// txtProcess
			// 
			this->txtProcess->Location = System::Drawing::Point( 82, 71 );
			this->txtProcess->Name = L"txtProcess";
			this->txtProcess->Size = System::Drawing::Size( 189, 20 );
			this->txtProcess->TabIndex = 1;
			// 
			// txtDllPath
			// 
			this->txtDllPath->Location = System::Drawing::Point( 82, 97 );
			this->txtDllPath->Name = L"txtDllPath";
			this->txtDllPath->Size = System::Drawing::Size( 189, 20 );
			this->txtDllPath->TabIndex = 2;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point( 4, 74 );
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size( 76, 13 );
			this->label1->TabIndex = 3;
			this->label1->Text = L"Process Name";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point( 28, 100 );
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size( 52, 13 );
			this->label2->TabIndex = 4;
			this->label2->Text = L"DLL Path";
			// 
			// pictureBox1
			// 
			this->pictureBox1->ErrorImage = ( cli::safe_cast< System::Drawing::Image^ >( resources->GetObject( L"pictureBox1.ErrorImage" ) ) );
			this->pictureBox1->Image = ( cli::safe_cast< System::Drawing::Image^ >( resources->GetObject( L"pictureBox1.Image" ) ) );
			this->pictureBox1->ImageLocation = L"";
			this->pictureBox1->InitialImage = ( cli::safe_cast< System::Drawing::Image^ >( resources->GetObject( L"pictureBox1.InitialImage" ) ) );
			this->pictureBox1->Location = System::Drawing::Point( 7, 6 );
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size( 270, 55 );
			this->pictureBox1->TabIndex = 5;
			this->pictureBox1->TabStop = false;
			// 
			// txtError
			// 
			this->txtError->AutoSize = true;
			this->txtError->BackColor = System::Drawing::Color::Transparent;
			this->txtError->ForeColor = System::Drawing::Color::FromArgb( static_cast< System::Int32 >( static_cast< System::Byte >( 255 ) ), static_cast< System::Int32 >( static_cast< System::Byte >( 128 ) ),
																		  static_cast< System::Int32 >( static_cast< System::Byte >( 128 ) ) );
			this->txtError->Location = System::Drawing::Point( 28, 128 );
			this->txtError->MaximumSize = System::Drawing::Size( 180, 0 );
			this->txtError->MinimumSize = System::Drawing::Size( 180, 0 );
			this->txtError->Name = L"txtError";
			this->txtError->Size = System::Drawing::Size( 180, 13 );
			this->txtError->TabIndex = 6;
			this->txtError->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// txtSuccess
			// 
			this->txtSuccess->Anchor = System::Windows::Forms::AnchorStyles::Right;
			this->txtSuccess->AutoSize = true;
			this->txtSuccess->BackColor = System::Drawing::Color::Transparent;
			this->txtSuccess->ForeColor = System::Drawing::Color::LimeGreen;
			this->txtSuccess->ImageAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->txtSuccess->Location = System::Drawing::Point( 21, 128 );
			this->txtSuccess->MinimumSize = System::Drawing::Size( 180, 0 );
			this->txtSuccess->Name = L"txtSuccess";
			this->txtSuccess->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->txtSuccess->Size = System::Drawing::Size( 180, 13 );
			this->txtSuccess->TabIndex = 7;
			this->txtSuccess->Text = L"Injected!";
			this->txtSuccess->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->txtSuccess->Visible = false;
			// 
			// MainForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF( 6, 13 );
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size( 284, 157 );
			this->Controls->Add( this->txtSuccess );
			this->Controls->Add( this->txtError );
			this->Controls->Add( this->pictureBox1 );
			this->Controls->Add( this->label2 );
			this->Controls->Add( this->label1 );
			this->Controls->Add( this->txtDllPath );
			this->Controls->Add( this->txtProcess );
			this->Controls->Add( this->bntInject );
			this->MaximizeBox = false;
			this->MaximumSize = System::Drawing::Size( 292, 185 );
			this->MinimizeBox = false;
			this->MinimumSize = System::Drawing::Size( 292, 185 );
			this->Name = L"MainForm";
			this->ShowIcon = false;
			this->ShowInTaskbar = false;
			this->Text = L"Injector";
			this->TopMost = true;
			( cli::safe_cast< System::ComponentModel::ISupportInitialize^ >( this->pictureBox1 ) )->EndInit();
			this->ResumeLayout( false );
			this->PerformLayout();

		}
#pragma endregion
	
private: System::Void bntInject_Click( System::Object^  sender, System::EventArgs^  e )
{
	txtError->Visible = true;
	txtSuccess->Visible = false;

	txtError->Text = "";

	Injector inject;
	DWORD dwProcessId = GetCurrentProcessId();
	const char* szProcess  = ( const char* )( Marshal::StringToHGlobalAnsi( txtProcess->Text ) ).ToPointer();
	const char* szDllPath = ( const char* )( Marshal::StringToHGlobalAnsi( txtDllPath->Text ) ).ToPointer();

	if( strlen( szProcess ) == 0 )
	{
		txtError->Text = "Please enter a process name!";
		return;
	}

	if( strlen( szDllPath ) == 0 )
	{
		txtError->Text = "Please enter a DLL path!";
		return;
	}

	if( strlen( szProcess ) > 0 )
	{
		if( dwProcessId == GetCurrentProcessId() )
		{ 
			dwProcessId = inject.GetProcessIdFromProcessName( szProcess );

			if( dwProcessId == GetCurrentProcessId() )
			{
				txtError->Text = "Failed to obtain process";
				return;
			}
		}
	}

	HMODULE hKernel = LoadLibraryA( "kernel32.dll" );
	DWORD64 dwLoadLibraryA = ( DWORD64 )GetProcAddress( hKernel, "LoadLibraryA" ) - ( DWORD64 )hKernel;

	char szCurrentModulePath[ MAX_PATH ] = { 0 };

	GetModuleFileNameA( GetModuleHandle( NULL ), szCurrentModulePath, MAX_PATH );

	for( size_t i = strlen( szCurrentModulePath ); i > 0; i-- )
	{
		if( szCurrentModulePath[ i ] == '\\' )
		{
			szCurrentModulePath[ i + 1 ] = 0;
			break;
		}
	}

	strcat_s( szCurrentModulePath, szDllPath );

	DWORD dwFileAttributes = GetFileAttributesA( szCurrentModulePath );

	if( dwFileAttributes == INVALID_FILE_ATTRIBUTES && GetLastError() == ERROR_FILE_NOT_FOUND )
	{
		txtError->Text = "File not found...";
		return;
	}

	HMODULE hRemoteKernel = inject.GetRemoteModuleHandleA( dwProcessId, "kernel32.dll" );

	if( hRemoteKernel == NULL )
	{
		txtError->Text = "Failed to locate kernel32 in remote process...";
		return;
	}

	HANDLE hProcess = OpenProcess( PROCESS_ALL_ACCESS, FALSE, dwProcessId );

	if( hProcess == INVALID_HANDLE_VALUE )
	{
		txtError->Text = "Failed to locate remote process...";
		return;
	}

	LPVOID lpModuleName = VirtualAllocEx( hProcess, NULL, strlen( szCurrentModulePath ) + 1, MEM_COMMIT, PAGE_EXECUTE_READWRITE );

	if( lpModuleName == NULL )
	{
		txtError->Text = "Failed to allocate module name in remote process...";
		return;
	}

	if( WriteProcessMemory( hProcess, lpModuleName, szCurrentModulePath, strlen( szCurrentModulePath ), NULL ) == FALSE )
	{
		txtError->Text = "Failed to write module name in remote process...";
		return;
	}

	DWORD64 dwRemoteLoadLibraryAddress = ( ( DWORD64 )hRemoteKernel + dwLoadLibraryA );

	HANDLE hThread = CreateRemoteThread( hProcess, 0, 0, ( LPTHREAD_START_ROUTINE )dwRemoteLoadLibraryAddress, lpModuleName, 0, 0 );

	txtError->Text = "Injecting...";

	WaitForSingleObject( hThread, INFINITE );

	txtError->Visible = false;
	txtSuccess->Visible = true;
	return;
}

};
}
